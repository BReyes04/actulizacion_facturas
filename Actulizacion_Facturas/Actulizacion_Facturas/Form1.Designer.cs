﻿namespace Actulizacion_Facturas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.comBoxVendedor = new System.Windows.Forms.ComboBox();
            this.LblVenActual = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnActulizar = new System.Windows.Forms.Button();
            this.textBoxRazon = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // comBoxVendedor
            // 
            this.comBoxVendedor.FormattingEnabled = true;
            this.comBoxVendedor.Location = new System.Drawing.Point(188, 61);
            this.comBoxVendedor.Name = "comBoxVendedor";
            this.comBoxVendedor.Size = new System.Drawing.Size(327, 28);
            this.comBoxVendedor.TabIndex = 0;
            // 
            // LblVenActual
            // 
            this.LblVenActual.AutoSize = true;
            this.LblVenActual.Location = new System.Drawing.Point(12, 9);
            this.LblVenActual.Name = "LblVenActual";
            this.LblVenActual.Size = new System.Drawing.Size(51, 20);
            this.LblVenActual.TabIndex = 1;
            this.LblVenActual.Text = "label1";
            this.LblVenActual.Click += new System.EventHandler(this.LblVenActual_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Vendor nuevo";
            // 
            // BtnActulizar
            // 
            this.BtnActulizar.Location = new System.Drawing.Point(173, 207);
            this.BtnActulizar.Name = "BtnActulizar";
            this.BtnActulizar.Size = new System.Drawing.Size(156, 45);
            this.BtnActulizar.TabIndex = 3;
            this.BtnActulizar.Text = "Actulizar";
            this.BtnActulizar.UseVisualStyleBackColor = true;
            this.BtnActulizar.Click += new System.EventHandler(this.BtnActulizar_Click);
            // 
            // textBoxRazon
            // 
            this.textBoxRazon.Location = new System.Drawing.Point(62, 131);
            this.textBoxRazon.Multiline = true;
            this.textBoxRazon.Name = "textBoxRazon";
            this.textBoxRazon.Size = new System.Drawing.Size(403, 53);
            this.textBoxRazon.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 277);
            this.Controls.Add(this.textBoxRazon);
            this.Controls.Add(this.BtnActulizar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LblVenActual);
            this.Controls.Add(this.comBoxVendedor);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comBoxVendedor;
        private System.Windows.Forms.Label LblVenActual;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnActulizar;
        private System.Windows.Forms.TextBox textBoxRazon;
    }
}

