﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Actulizacion_Facturas
{
    public partial class Form1 : Form
    {
        public static class Compania
        {
            public static string CodCompania;
        }
        public static class RowOC
        {
            public static string RowOCCod;
        }
        public static class Factura
        {
            public static string FacturaCod;
        }
        public static class connetionString
        {
            public static string connetionStringC;
        }
        public static class User
        {
            public static string CodUSer;
        }
        public static class Based
        {
            public static string BaseDatos;
        }
        public Form1(string Com, string row, string user,string Base)
        {

            Compania.CodCompania = Com;
            RowOC.RowOCCod = row;
            User.CodUSer = user;
            Based.BaseDatos = Base;
            InitializeComponent();
            connetionString.connetionStringC = "Data Source=192.168.7.2;Initial Catalog="+Based.BaseDatos.ToString()+";Persist Security Info=True;User ID=sa;Password=jda";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(connetionString.connetionStringC))
                {
                    string query = "SELECT VENDEDOR,NOMBRE FROM " + Compania.CodCompania.ToString() + ".VENDEDOR WHERE ACTIVO='S'";
                    string query2 = "SELECT  f.FACTURA,f.VENDEDOR,v.NOMBRE FROM " + Compania.CodCompania.ToString() + ".FACTURA f  INNER JOIN " + Compania.CodCompania.ToString() + ".VENDEDOR v ON f.VENDEDOR=v.VENDEDOR WHERE  f.RowPointer='" + RowOC.RowOCCod.ToString() + "'";

                    SqlCommand cmd = new SqlCommand(query, conn);
                    SqlCommand cmd2 = new SqlCommand(query2, conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                    da.Fill(dt);
                    da2.Fill(dt2);
                }
                DataRow[] rows = dt2.Select();
                comBoxVendedor.DisplayMember = "NOMBRE";
                comBoxVendedor.ValueMember = "VENDEDOR";
                comBoxVendedor.DataSource = dt;
                Factura.FacturaCod = rows[0]["FACTURA"].ToString();
                LblVenActual.Text = "Factura: " + rows[0]["FACTURA"].ToString() + " Vendedor Acutal: " + rows[0]["NOMBRE"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void LblVenActual_Click(object sender, EventArgs e)
        {

        }

        private void BtnActulizar_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connetionString.connetionStringC))
            {
                try
                {
                    conn.Open();
                    string query = "EXECUTE dbo.In_soli_Act_vendedor @PAIS = '" + Compania.CodCompania.ToString() + "', @Factura = '" + Factura.FacturaCod.ToString() + "', @Vendedor = '" + comBoxVendedor.SelectedValue.ToString() + "', @usuario = '" + User.CodUSer.ToString() + "', @razonSo = '" + textBoxRazon.Text + "'";

                    SqlCommand cmd = new SqlCommand(query, conn);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.InsertCommand = cmd;
                    da.InsertCommand.ExecuteNonQuery();
                    conn.Close();
                    MessageBox.Show("Solicitud ingresada Exitosamente");
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
        }
    }
}
